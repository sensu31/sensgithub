- 👋 Hi, I’m Svetlan!
- 👀 Interested in databases, automation, scripting, containerisation etc. 
- 🌱 Passionate learner with a thirst for knowledge!
- 📫 How to reach me: [LinkedIn](https://www.linkedin.com/mwlite/in/svetlan-nikolov-0323b2238) | svetlan.nikolov@yahoo.com

<!---
sensgithub/sensgithub is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
